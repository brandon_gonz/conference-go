from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"query": f"{city} {state}"}
    headers = {"Authorization":PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=params).json()
    return response["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/data/2.5/weather"
    query = f"{city}, {state}, 1"
    params = {"q": query, "appid": OPEN_WEATHER_API_KEY, "units": "imperial"}
    response = requests.get(url,  params=params)
    if response.status_code == 200:
        try:
            response_json = response.json()
            weather = {"description": response_json["weather"][0]["description"], "temperature": round(
                response_json["main"]["temp"])}
            return weather
        except (KeyError):
            return None
    else:
        return None
